
@extends('master',['title'=>"تعديل معلومات العميل"])
@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 style="margin:0px; padding:15px;" class="page-header">إدارة الموقع الإلكتروني</h1><!-- /.col-lg-12 -->
            </div>
            <div class="modal-body">
                <p style="font-size: 150%">عنوان الصفحة</p>

                <form action="{{asset(url(env('CMS_NAME').'/update-publisher'))}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">عربي</label>
                            <input type="text" class="form-control" id="recipient-name" name="title" value="{{$menus->title}}" required>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label"> </label>
                            <input type="file" class="form-control" id="recipient-name" name="image" >
                            @if($menus->image != null)
                                <img src="{{asset('asset/images/'.$menus->image)}}" width="100" height="100">
                            @endif
                        </div>
                    </div>
                     <div class="col-lg-4">
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label"> </label>
                            <input type="file" class="form-control" id="recipient-name" name="pdf" >
                            @if($menus->file != null)
                                <p>{{$menus->file}}</p>
                            @endif
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="adjoined-bottom">
                            <div class="grid-container">
                                <div class="grid-width-100">
                                <textarea id="editor" name="description">
                                    {{$menus->description}}
                                </textarea>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div style="text-align: right" class="modal-footer">
                        <button type="submit" class="btn btn-primary">حفظ</button>
                    </div>
                    <div class="form-group">
                        <input required type="hidden" class="form-control" id="recipient-name" name="id" value="{{$menus->id}}">
                    </div>
                </form>
            </div>
        </div>
        <script>
            initSample();
        </script>
@stop

<?php


namespace App\Repositories\api;

use App\Exceptions\AppException;
use App\Models\Category;
use App\Repositories\BaseRepository;
use DB;
use Exception;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;


class categoriesRepository extends BaseRepository
{
    public function model()
    {
        return Category::class;
    }

    /**
     * @inheritDoc
     */
    public function relations()
    {
        return [
            'products'
        ];
    }

    public function create(array $data)
    {
        DB::beginTransaction();


        $categories = parent::create($data);

        if (!empty($data['products'] ?? [])) {
            $categories->roles()->sync($data['products']);
        }

        DB::commit();
        return $categories;
    }

    public function updateById($id, array $data, array $options = [])
    {
        DB::beginTransaction();


        $categories = parent::updateById($id, $data);

        if (!empty($data['products'] ?? [])) {
            $categories->roles()->sync($data['products']);
        }

        DB::commit();
        return $categories->refresh();
    }
}

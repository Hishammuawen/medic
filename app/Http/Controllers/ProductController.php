<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;
use DB;
class ProductController extends Controller
{


    public function index()
    {
        $all=DB::table('products')->get();
        return view('Dashbord/product',compact('all'));
    }


    public function create()
    {
        return view('Dashbord/addProduct');
    }


    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'type' => 'required',
            'color' => 'required',
            'description' => 'required',
            'production_date' => 'required',
            'expiry_date' => 'required',
        ]);

        DB::table('products')->insert(
            ['name' =>$request->name ,
            'type' =>$request->type ,
            'color' =>$request->color ,
                'description' => $request->description,
                'production_date' => $request->production_date,
                'expiry_date'=>$request->expiry_date]
        );

        return redirect('allproduct');
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $product = Product::findOrFail($id);
       // dd($product);
        return view('Dashbord/editProduct',compact('product'));
    }


    public function update(Request $request,$id)
    {
        $product = Product::findOrFail($id);
        $product->name=$request['name'];
        $product->type=$request['type'];
        $product->color=$request['color'];
        $product->description=$request['description'];
        $product->production_date=$request['production_date'];
        $product->expiry_date=$request['expiry_date'];
        $product->save();

        return redirect('allproduct');
    }


    public function destroy($id)
    {
        DB::table('products')->where('id', '=',$id)->delete();

        return redirect('allproduct');
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\AboutUs;
use Illuminate\Http\Request;
use DB;
class AboutusController extends Controller
{
    public function index()
    {
        $about1 = AboutUs::all();
        if ($about1->isEmpty())
            return view('dashboard.aboutus');
        else {
            $about = $about1[0];
            return view('dashboard.aboutus', compact('about'));
        }
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {

      $about  =  AboutUs::find(1);
      if($about == null){
		  $about = new AboutUs();
	  }
      $about["title"]=$request->get("title");
      $about["description"]=$request->get("description");
     if($request->file('image') != null){

                $image = $request->file('image');
                $filename = time() . '.' . $image->getClientOriginalExtension();
                $path = public_path('asset/images/');
                $image->move($path, $filename);
                $about['image'] = $filename;
            }
      $about->save();

      return \Redirect::back();
    }

       public function show($id)
    {
        //
    }
    public function alert($AlertType){
        switch ($AlertType) {
            case "success":
                Alert::success("this is success alert");
                dd(alert()->message('Message', 'Optional Title'));
return redirect("/");
break;

            default:
                return redirect("/");
                break;
        }
    }
    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}

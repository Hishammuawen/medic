
@extends('master')
@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 style="margin:0px; padding:15px;" class="page-header">إدارة الموقع الإلكتروني</h1><!-- /.col-lg-12 -->
            </div>

            <div class="row">
			<br>
                <div class="modal-body">
                    <form action="{{asset(url(env('CMS_NAME').'/save-contact'))}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label"> رقم الهاتف </label>
                                @if(isset($about->phone))
                                   <input type="number" class="form-control" id="recipient-name" value="{{$about->phone}}" name="phone" required>
                           @else
                                    <input type="number" class="form-control" id="recipient-name" value="" name="phone" required>

                                @endif
                            </div>
                        </div>
						        <div class="col-lg-4">
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label"> موقع الشركة </label>
                                @if(isset($about->location))
                                   <input type="text" class="form-control" id="recipient-name" value="{{$about->location}}" name="location" required>
                           @else
                                    <input type="text" class="form-control" id="recipient-name" value="" name="location" required>

                                @endif
                            </div>
                        </div>
						                       
						<div class="col-lg-4">
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label"> ايميل الشركة</label>
                                @if(isset($about->email))
                                   <input type="email" class="form-control" id="recipient-name" value="{{$about->email}}" name="email" required>
                           @else
                                    <input type="email" class="form-control" id="recipient-name" value="" name="email" required>
                                @endif
                            </div>
                        </div>

                      
                            <div style="text-align: right" class="modal-footer">
                            <button type="submit" class="btn btn-primary">حفظ</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
@stop

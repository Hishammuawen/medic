<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\ApiController;
use App\Http\Requests\api\ContactUs\contactUsDestroyRequest;
use App\Http\Requests\api\ContactUs\contactUsUpdateRequest;
use App\Http\Requests\api\ContactUs\contactUsIndexRequest;
use App\Http\Requests\api\ContactUs\contactUsShowRequest;
use App\Http\Requests\api\ContactUs\contactUsStoreRequest;
use App\Repositories\api\contactUsRepository;

class ContactUsController extends ApiController
{

    protected $contactUs_repository;

    public function __construct(contactUsRepository $contactUs_repository)
    {
        parent::__construct();
        $this->contactUs_repository = $contactUs_repository;
    }


      public function index(contactUsIndexRequest $request)
    {
        return $this->respondWithPagination($this->contactUs_repository->searchableIndex($request->validated(),
            $this->getLimit()));

    }


    public function store(contactUsStoreRequest $request)
    {
        return $this->respond($this->contactUs_repository->create($request->validated()));
    }

    public function show(contactUsShowRequest $request,$id)
    {
        return $this->respond($this->contactUs_repository->getById($id));

    }


    public function update(contactUsUpdateRequest $request, $id)
    {
        return $this->respond($this->contactUs_repository->updateById($id, $request->validated()));
    }


    public function destroy(contactUsDestroyRequest $request,$id)
    {
        if ($this->contactUs_repository->softDeleteById($id)) {
            return $this->respondDeletedSuccessfully('contactUs Deleted Successfully', 'تم حذف اتصل بنا  بنجاح');
        }

    }
}

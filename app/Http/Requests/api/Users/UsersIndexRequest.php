<?php

namespace App\Http\Requests\api\Users;

use Illuminate\Foundation\Http\FormRequest;

class UsersIndexRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('index_user');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => [
                'nullable',
                'string'
            ],
            'last_name' => [
                'nullable',
                'string'
            ],
            'email' => [
                'nullable',
                'string'
            ],
            'roles_name' => [
                'nullable',
                'string'
            ],
            'roles_display_name' => [
                'nullable',
                'string'
            ],
        ];
    }
}

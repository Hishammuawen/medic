<?php

namespace App\Http\Controllers;

use App\Models\ContactUs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;
use DB;
class ContactusController extends Controller
{
    public function index(){
        $about=DB::table('contacts')->get();

$about=$about[0];        return view('dashboard.contacts', compact('about'));
    }


    public function create()
    {
        return view('Dashbord/addcontactus');
    }

    public function store(Request $request)
    {
    $about  =  ContactUs::find(1);
      if($about == null){
		  $about = new ContactUs();
	  }
      $about["phone"]=$request->get("phone");
      $about["location"]=$request->get("location");
	  $about["email"]=$request->get("email");
      $about->save();

      return redirect()->route("contacts");

    }

    public function show($id)
    {
        //
    }
    public function edit($id)
    {
        $contactus = ContactUs::findOrFail($id);
        // dd();
        return view('Dashbord/editcontactus',compact('contactus'));
    }

    public function update(Request $request, $id)
    {
        $contactus = ContactUs::findOrFail($id);
        $contactus->phone=$request['phone'];
        $contactus->email=$request['email'];
        $contactus->location=$request['location'];

        $contactus->save();

        return redirect('allcontactus');
    }

    public function destroy($id)
    {
        DB::table('contact_us')->where('id', '=',$id)->delete();

        return redirect('allcontactus');
    }
}

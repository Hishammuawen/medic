<?php

namespace App\Http\Requests\api\Users;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('update_user');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => [
                'string'
            ],
            'last_name' => [
                'string'
            ],
            'email' => [
                'email'
            ],
            'password' => [
                'string'
            ],
            'roles' => [
                'nullable',
                'array'
            ],
            'roles.*' => [
                'required',
                'integer',
                Rule::exists('roles', 'id')->whereNull('deleted_at')
            ]
        ];
    }
}

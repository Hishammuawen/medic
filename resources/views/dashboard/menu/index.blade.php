
@extends('master',['title'=>"القوائم"])
@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div style=" margin-bottom: 20px" class="col-lg-12">
                <h1 style="margin:0px; padding:15px;" class="page-header">إدارة الموقع الإلكتروني</h1><!-- /.col-lg-12 -->
            </div>
        <br>
        <br>
        <!-- /.row -->
        <div style="margin-top: 20px;" class="row">
            <div class="col-lg-12">


                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                <tr>


                                    <th>الاسم بالعربي </th>
                                    <th>المستوى</th>
                                    <th>ترتيب الظهور</th>
                                    <th>العمليالت</th>

                                </tr>
                                </thead>
                                <tbody>
                                @if(isset($menus))
                                @foreach($menus as $r)
                                    <tr class="odd gradeX">

                                        <td>{{$r->title}}</td>
                                        @if($r->type == 1)
                                        <td>أساسي</td>
                                        @else
                                            <td>فرعي</td>
                                            @endif
                                        <td>{{$r->	Priority}}</td>

                                        <td>

                                            <button type="button" class="btn btn-primary btn-circle">
                                                <a style="color: white" href="{{asset(url(env('CMS_NAME').'/edit-element/'.$r->id))}}" aria-labelledby="myModalLabel1" aria-hidden="true">
                                                    <i class="fa fa-bars"></i></a>
                                            </button>

                                            <button type="button" class="btn btn-danger btn-circle"   id="btn-confirm"><a  style="color: white"  href="{{asset(url(env('CMS_NAME').'/delete-element/'.$r->id))}}" aria-labelledby="myModalLabel1" aria-hidden="true"><i class="fa fa-times"></i></a>
                                            </button>

                                            </li>

                                        </td>

                                    </tr>
                                @endforeach
                                    @endif
                                </tbody>
                            </table>
                        <!-- /.table-responsive -->


            </div>
            <!-- /.col-lg-12 -->
        </div>



@stop



















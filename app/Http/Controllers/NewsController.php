<?php

namespace App\Http\Controllers;

use App\Models\News;
use Illuminate\Http\Request;
use DB;

class NewsController extends Controller
{

    public function index()
    {
        $all=DB::table('news')->get();
        return view('Dashbord/news',compact('all'));
    }


    public function create()
    {
        return view('Dashbord/addnew');
    }


    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'title' => 'required',
            'shortcut_news' => 'required',
            'description' => 'required',
            'date' => 'required'
        ]);

       DB::table('news')->insert(
            ['name' =>$request->name ,
                'title' =>$request->title ,
                'shortcut_news' =>$request->shortcut_news ,
                'description' => $request->description,
                'date' => $request->date
                ]
        );
     //  dd($a);

        return redirect('allnews');
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $new = News::findOrFail($id);
        // dd();
        return view('Dashbord/editnew',compact('new'));
    }


    public function update(Request $request, $id)
    {
        $new = News::findOrFail($id);
        $new->name=$request['name'];
        $new->title=$request['title'];
        $new->shortcut_news=$request['shortcut_news'];
        $new->description=$request['description'];
        $new->date=$request['date'];
        $new->save();

        return redirect('allnews');
    }


    public function destroy($id)
    {
        DB::table('news')->where('id', '=',$id)->delete();

        return redirect('allnews');
    }
}

<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\ApiController;
use App\Http\Requests\api\Products\productsDestroyRequest;
use App\Http\Requests\api\Products\productsIndexRequest;
use App\Http\Requests\api\Products\productsShowRequest;
use App\Http\Requests\api\Products\productsStoreRequest;
use App\Http\Requests\api\Products\productsUpdateRequest;
use App\Repositories\api\productsRepository;

class ProductController extends ApiController
{
    protected $products_repository;

    public function __construct(productsRepository $products_repository)
    {
        parent::__construct();
        $this->products_repository = $products_repository;
    }


    public function index(productsIndexRequest $request)
    {
        return $this->respondWithPagination($this->products_repository->searchableIndex($request->validated(),
            $this->getLimit()));

    }

       public function store(productsStoreRequest $request)
    {
        return $this->respond($this->products_repository->create($request->validated()));
    }

    public function show(productsShowRequest $request,$id)
    {
        return $this->respond($this->products_repository->getById($id));
    }

    public function update(productsUpdateRequest $request,$id)
    {
        return $this->respond($this->products_repository->updateById($id, $request->validated()));
    }


    public function destroy(productsDestroyRequest $request,$id)
    {
        if ($this->products_repository->softDeleteById($id)) {
            return $this->respondDeletedSuccessfully('products Deleted Successfully', 'تم حذف المنتجات  بنجاح');
        }

    }
}

<?php

namespace App\Http\Requests\api\Events;

use Illuminate\Foundation\Http\FormRequest;

class eventsStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('store_events');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'nullable',
                'string'
            ],
            'title' => [
                'nullable',
                'string'
            ],
            'shortcut_news' => [
                'nullable',
                'string'
            ],
            'description' => [
                'nullable',
                'string'
            ],
            'date' => [
                'nullable',
                'date'
            ],

        ];
    }
}


@extends('master',['title'=>"الصفحات"])
@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 style="margin:0px; padding:15px;" class="page-header">إدارة الموقع الإلكتروني</h1><!-- /.col-lg-12 -->
            </div>

        <!-- /.row -->
        <div style="padding-top: 20px" class="row">
            <br>

            <div class="col-lg-12">
                <h2> إدارة الزبائن</h2>
                    <!-- /.panel-heading -->

                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                <tr>

                                    <th>عنوان بالعربي</th>
                                    <th>الرابط</th>
                                    <th>العمليات</th>
                                </tr>
                                </thead>
                                <tbody>
                                @if(isset($pages))
                                @foreach($pages as $r)
                                    <tr class="odd gradeX">

                                        <td>{{$r->title}}</td>
                                        <td>{!! $r->description !!}</td>
                                        <td>
                                            <button type="button" class="btn btn-primary btn-circle">
                                                <a style="color: white" href="{{asset(url(env('CMS_NAME').'/edit-customer/'.$r->id))}}" aria-labelledby="myModalLabel1" aria-hidden="true">
                                                    <i  data-toggle="tooltip" title="التعديل"  class="fa fa-bars"></i></a>
                                            </button>
                                            <button type="button" class="btn btn-danger btn-circle"   id="btn-confirm"><a style="color: white"  href="{{asset(url(env('CMS_NAME').'/delete-customer/'.$r->id))}}" aria-labelledby="myModalLabel1" aria-hidden="true"><i  data-toggle="tooltip" title="الحذف" class="fa fa-times"></i></a>
                                            </button>
                                        </td>



                                    </tr>
                                @endforeach
                                    @endif
                                </tbody>
                            </table>
                        <!-- /.table-responsive -->

            </div>
            <!-- /.col-lg-12 -->
        </div>



@stop



















<?php

namespace App\Http\Requests\api\Products;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class productsIndexRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('index_products');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'nullable',
                'string'
            ],
            'type' => [
                'nullable',
                'string'
            ],
            'color' => [
                'nullable',
                'string'
            ],
            'description' => [
                'nullable',
                'string'
            ],
            'production_date' => [
                'nullable',
                'date'
            ],
            'expiry_date' => [
                'nullable',
                'date'
            ],
            'category' => [
                'nullable',
                'array'
            ],
            'category.*' => [
                'required',
                'integer',
                Rule::exists('users', 'id')
            ]


        ];
    }
}

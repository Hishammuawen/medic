
@extends('master',['title'=>"أضافة عميل جديد"])
@section('content')

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 style="margin:0px; padding:15px;" class="page-header">إدارة الموقع الإلكتروني</h1><!-- /.col-lg-12 -->
            </div>
            <div class="modal-body">
                <p style="font-size: 150%">  إضافة عميل جديد</p>

                <form action="{{asset(url(env('CMS_NAME').'/save-publisher'))}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">اسم العميل</label>
                            <input type="text" class="form-control" id="recipient-name" name="title" required>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label"> الصورة</label>
                            <input type="file" class="form-control" id="recipient-name" name="image">
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label"> الملف</label>
                            <input type="file" class="form-control" id="recipient-name" name="pdf">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">شرح عن العميل</label>
                        <div class="adjoined-bottom">
                            <div class="grid-container">
                                <div class="grid-width-100">
				<textarea id="editor" name="description">
			</textarea>
                                </div>
                            </div>
                        </div>                </div>
                    <div style="text-align: right" class="modal-footer">
                        <button type="submit" class="btn btn-primary">حفظ</button>
                    </div>
                </form>
            </div>
        </div>
        <script>
            initSample();
        </script>
@stop

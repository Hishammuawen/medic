<?php
//
//Route::get('/', function () {
//    return view('dashboard/home')->name("login");
//});


Route::get('/', function () {
    return view('site/home');
})->name("home");

//]);
Route::get('logout', '\App\Http\Controllers\Auth\LoginController@logout')->name("logout");

Route::post('login', 'ApiController@login');
Route::post('register', 'ApiController@register');
Route::get('/admin', 'ApiController@login')->name("login");
Route::post('/loginDashboard', 'Auth\LoginController@login');
Route::get('/page/{id}/{title}','siteController@load');
Route::get('/section/{id}/{title}','siteController@loadSection');
Route::get('/component-pages/{id}/{title}','siteController@loadDetail');

Route::group(['prefix' => env('CMS_NAME'),'middleware'=>'auth'],function() {

    /******* Routes Menu **/
    Route::get("/add-new-menu", 'MenuController@create');
    Route::post("/save-element", 'MenuController@store');
    Route::get("/edit-element/{id}", 'MenuController@edit');
    Route::post("/update-element", 'MenuController@update');
    Route::get("/delete-element/{id}", 'MenuController@destroy');
    Route::get("/all-elements-menu", 'dashboardController@index')->name("all-elements-menu");
    /****** End Routes Menu */

    /******* Routes customer **/
        Route::get("/add-new-customer", 'customerController@create');
        Route::post("/save-customer", 'customerController@store');
        Route::get("/edit-customer/{id}", 'customerController@edit');
        Route::get("/edit-content-customer/{id}", 'customerController@editContent')->name('edit-content-customer');
        Route::post("/update-customer", 'customerController@update');
        Route::get("/delete-customer/{id}", 'customerController@destroy');
        Route::get("/all-customer", 'customerController@index')->name("all-customer");
    /****** End Routes Page */

    /******* Routes component **/
    Route::get("/add-new-publisher", 'publisherController@create');
    Route::post("/save-publisher", 'publisherController@store');
    Route::get("/edit-publisher/{id}", 'publisherController@edit');
    Route::post("/update-publisher", 'publisherController@update');
    Route::get("/delete-publisher/{id}", 'publisherController@destroy');
    Route::get("/all-publisher", 'publisherController@index')->name("all-publisher");
    /****** End Routes component */


    /******* Routes service **/
    Route::get("/add-new-service", 'serviceController@create');
    Route::post("/save-service", 'serviceController@store');
    Route::get("/edit-service/{id}", 'serviceController@edit');
    Route::get("/edit-content-service/{id}", 'serviceController@editContent');
    Route::post("/update-service", 'serviceController@update');
    Route::get("/delete-service/{id}", 'serviceController@destroy');
    Route::get("/all-elements-service", 'serviceController@index')->name("all-elements-service");
    /****** End Routes section */

    /******* Routes component **/
    Route::get("/add-new-component", 'componentController@create');
    Route::post("/save-component", 'componentController@store');
    Route::get("/edit-component/{id}", 'componentController@edit');
    Route::get("/edit-content-component/{id}", 'componentController@editContent');
    Route::post("/update-component", 'componentController@update');
    Route::get("/delete-component/{id}", 'componentController@destroy');
    Route::get("/all-elements-component", 'componentController@index')->name("all-elements-component");
    /****** End Routes section */
    /******* Routes component **/
    Route::get("/add-new-items", 'itemsController@create');
    Route::post("/save-items", 'itemsController@store');
    Route::get("/edit-items/{id}", 'itemsController@edit');
    Route::get("/edit-content-items/{id}", 'itemsController@editContent');
    Route::post("/update-items", 'itemsController@update');
    Route::get("/delete-items/{id}", 'itemsController@destroy');
    Route::get("/all-elements-items", 'itemsController@index')->name("all-elements-component");
    /****** End Routes section */



    Route::get('home', 'Controller@home');

    /************ Users Route */
    Route::get('/about-us', 'AboutusController@index');
    Route::post('/save-about', 'AboutusController@store');
	
	Route::get('/contact-us', 'contactusController@index')->name("contacts");
    Route::post('/save-contact', 'contactusController@store');
	
    Route::get('/alert/{AlertType}', 'AboutusController@alert')->name('alert');
    Route::get('all-users', 'UserController@index')->name("all-users");
    Route::get('/add-user', 'UserController@create');
    Route::post('/save-user', 'UserController@store');
    route::get('/delete-user/{id}', 'UserController@destroy');
    route::get('/edit-user/{id}', 'UserController@edit');
    route::post('/update-user/{id}', 'UserController@update');
/************* End Users **/
});


Route::get('/home', 'HomeController@index')->name('home');

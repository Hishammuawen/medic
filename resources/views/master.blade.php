<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
@if(isset($title))
    <title>{{$title}}</title>
    @endif
    <!-- DataTables CSS -->
    <link href="{{asset('css/plugins/dataTables.bootstrap.css')}}" rel="stylesheet">

    <!-- Bootstrap Core CSS -->
    <link href="{{asset('css/rtl/bootstrap.min.css')}}" rel="stylesheet">

    <!-- not use this in ltr -->
    <link href="{{asset('css/rtl/bootstrap.rtl.css')}}" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="{{asset('css/plugins/metisMenu/metisMenu.min.css')}}" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="{{asset('css/plugins/timeline.css')}}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{asset('css/rtl/sb-admin-2.css')}}" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="{{asset('css/plugins/morris.css')}}" rel="stylesheet">
    <link href="{{asset('css/plugins/morris.css')}}" rel="stylesheet">
    <!-- Social Buttons CSS -->
    <link href="{{asset('sweet-alert/sweetalert.css')}}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{asset('css/font-awesome/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <link rel="{{asset('stylesheet" href="path/to/font-awesome/css/font-awesome.min .css')}}">
    <link href="{{asset('https://fonts.googleapis.com/css?family=Nunito:200,600')}}" rel="stylesheet">
    <link href="{{asset('css/rel.css')}}" rel="stylesheet">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

    <![endif]-->
    <script src="{{asset('ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('ckeditor/samples/js/sample.js') }}"></script>
    <link rel="stylesheet" href="{{asset('ckeditor/samples/css/samples.css')}}">
    <link rel="stylesheet" href="{{asset('ckeditor/samples/toolbarconfigurator/lib/codemirror/neo.css')}}">

</head>

<body>

<div id="wrapper" style="background-color: #263239 ">

    <!-- Navigation -->

        <!-- /.navbar-header -->


<style>
    ul li{
        backgroun-color:#263239;
        color: white;
    }
    a{
        color: #ffffff;
    }
    .nav>li>a {
    background-color: #222d32 ;
    }

</style>
        <div style="background-color: #263238; color: white" class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <li class="sidebar-search">
                        <div   class="input-group custom-search-form">
                           <img style="margin-top: -50px" height="100" width="200" src="{{asset('/images/logo.png')}}">
                        </div>
                        <!-- /input-group -->
                    </li>
                    <li>
                        <a href="#"><i class="fa  fa-book"></i>  إدارة المستخدمين <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="{{url( env('CMS_NAME')."/add-user")}}">إضافة مستخدم</a>
                            </li>
                            <li>
                                <a href="{{url( env('CMS_NAME').'/all-users')}}">إدارة المستخدمين</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>

                    <li>
                        <a href="#"><i class="fa  fa-book"></i>  الزبائن <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="{{url( env('CMS_NAME')."/add-new-customer")}}">أضافة زبون جديدة </a>
                            </li>
                            <li>
                                <a href="{{url( env('CMS_NAME').'/all-customer')}}">إدارة الزبائن</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="#"><i class="fa  fa-book"></i>   العملاء <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a  href="{{url( env('CMS_NAME')."/add-new-publisher")}}">أضافة عميل جديد  </a>
                            </li>
                            <li>
                                <a href="{{url( env('CMS_NAME')."/all-publisher")}}">إدارة العملاء</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="#"><i class="fa  fa-book"></i>    الخدمات <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a  href="{{url( env('CMS_NAME')."/add-new-service")}}">أضافة  خدمة جديدة  </a>
                            </li>
                            <li>
                                <a href="{{url( env('CMS_NAME')."/all-elements-service")}}">إدارة الخدمات</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="#"><i class="fa  fa-book"></i>    المكونات <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a  href="{{url( env('CMS_NAME')."/add-new-component")}}">أضافة  مكون جديدة  </a>
                            </li>
                            <li>
                                <a href="{{url( env('CMS_NAME')."/all-elements-component")}}">إدارة المكونات</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="#"><i class="fa  fa-book"></i>    المواد <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a  href="{{url( env('CMS_NAME')."/add-new-items")}}">أضافة  مادة  جديدة  </a>
                            </li>
                            <li>
                                <a href="{{url( env('CMS_NAME')."/all-elements-items")}}">إدارة المواد</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="#"><i class="fa  fa-book"></i>   إعدادات الموقع <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a  style="background-color:#263239;color: white;" href="{{url( env('CMS_NAME').'/about-us')}}"> من نحن</a>
                            </li>
							<li>
                                <a  style="background-color:#263239;color: white;" href="{{url( env('CMS_NAME').'/contact-us')}}">  تواصل معنا </a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="{{route('logout')}}" style="background-color:#263239;color: white;" class="active" href="{{url( env('CMS_NAME').'about-us')}}"><i class="fa fa-dashboard fa-book"></i> تسجيل الخروج</a>
                    </li>
                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->


    @yield('content')
</div>
<!-- /#wrapper -->
<!-- jQuery Version 1.11.0 -->
<script src="{{asset('js/jquery-1.11.0.js')}}"></script>

<!-- Bootstrap Core JavaScript -->
<script src="{{asset('js/bootstrap.min.js')}}"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="{{asset('js/metisMenu/metisMenu.min.js')}}"></script>

<!-- Morris Charts JavaScript -->
<script src="{{asset('js/raphael/raphael.min.js')}}"></script>
<script src="{{asset('js/morris/morris.min.js')}}"></script>

<!-- DataTables JavaScript -->
<script src="{{asset('js/jquery/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('js/bootstrap/dataTables.bootstrap.min.js')}}"></script>


<!-- Custom Theme JavaScript -->
<script src="{{ asset('js/sb-admin-2.js')}}"></script>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">

<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
@include('sweet::alert')


<!-- Page-Level Demo Scripts - Tables - Use for reference -->
<script>
    $(document).ready(function() {
        $('#dataTables-example').dataTable();
    });
</script>
</body>
</html>

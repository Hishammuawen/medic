<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Image extends Authenticatable
{
    protected $fillable = [
        'url', 'imageable_id', 'imageable_type',
    ];


    public function imageable()
    {
        return $this->morphTo();
    }
}

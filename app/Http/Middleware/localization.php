<?php

namespace App\Http\Middleware;

use App;
use Closure;
use Illuminate\Http\Request;
use Session;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

class localization extends BaseMiddleware

{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        dd(\Session::get("token"));
        if (!\Session::get("token")) {
            //   dd("dd2");
            return view("dashboard.login");
        }
        return view("site.home");
        dd("dd");
    }
}

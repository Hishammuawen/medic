<?php

namespace App\Http\Requests\api\ContactUs;

use Illuminate\Foundation\Http\FormRequest;

class contactUsIndexRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('index_contactUs');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => [
                'nullable',
                'string'
            ],
            'location' => [
                'nullable',
                'string'
            ],
            'email' => [
                'nullable',
                'string'
            ],

        ];
    }
}

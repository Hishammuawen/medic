<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Component;
use App\Menu;
use App\Page;
use Alert;
class dashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        return view("dashboard.home");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

     return  view("dashboard.menu.addMenu");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
   // dd($request->all());
        try {
            $menu = new Menu();
            $menu['title'] = $request->get('title');
            $menu['type'] = $request->get('type');
            $menu['priority'] = $request->get('priority');
            $menu['links'] = $request->get('link');
            $menu->save();
//           $route=app()->make('router');
//           $route->get("title/".$menu->id,'siteController@load');
            alert()->success('شكرا لك','تم إضافة عنصر جديد إلى القائمة');

            return redirect(env("CMS_NAME")."/all-elements-menu");

        }catch (\Exception $e){
            alert()->error(' الرجاء المحاولة التحقق من البيانات','لم يتم إضافة عنصر جديد إلى القائمة');
            return \Redirect::back()->with("Dd");
        }



    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $menus=Menu::find($id);
            return view("dashboard.menu.edit",compact("menus"));
        }catch (\Exception $exception){
return  \Redirect::back();
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {
            $menu = Menu::find($request->get("id"));
            $menu['title'] = $request->get('title');
            $menu['type'] = $request->get('type');
            $menu['Priority'] = $request->get('priority');
            $menu['links'] = $request->get('link');
            $menu->save();
            alert()->success('شكرا لك','تم تعديل العنصر ');
            return redirect(env("CMS_NAME")."/all-elements-menu");

        } catch (\Exception $e) {
            alert()->error(' حدث خطأ','لم يتم تعديل العنصر ');

        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $menu = Menu::find($id);
        $menu->delete();
            alert()->success('شكرا لك','تم حذف العنصر ');
            return redirect(env("CMS_NAME")."/all-elements-menu");
        } catch (\Exception $e) {
            alert()->success(' حدث خطأ','لم يتم حذف العنصر ');

        }
    }
}

<?php
namespace App\Http\Controllers\api;



use App\Http\Controllers\ApiController;
use App\Http\Requests\api\AboutUs\aboutUsDestroyRequest;
use App\Http\Requests\api\AboutUs\aboutUsIndexRequest;
use App\Http\Requests\api\AboutUs\aboutUsShowRequest;
use App\Http\Requests\api\AboutUs\aboutUsStoreRequest;
use App\Http\Requests\api\AboutUs\aboutUsUpdateRequest;
use App\Repositories\api\aboutUsRepository;

class AboutUsController extends ApiController
{

    protected $aboutUs_repository;

    public function __construct(aboutUsRepository $aboutUs_repository)
    {
        parent::__construct();
        $this->aboutUs_repository = $aboutUs_repository;
    }

    public function index(aboutUsIndexRequest $request)
    {
        return $this->respondWithPagination($this->aboutUs_repository->searchableIndex($request->validated(),
            $this->getLimit()));

    }


    public function store(aboutUsStoreRequest $request)
    {
        return $this->respond($this->aboutUs_repository->create($request->validated()));
    }

    public function show(aboutUsShowRequest $request,$id)
    {
        return $this->respond($this->aboutUs_repository->getById($id));
    }

    public function update(aboutUsUpdateRequest $request,$id)
    {
        return $this->respond($this->aboutUs_repository->updateById($id, $request->validated()));
    }


    public function destroy(aboutUsDestroyRequest $request, $id)
    {
        if ($this->aboutUs_repository->softDeleteById($id)) {
            return $this->respondDeletedSuccessfully('aboutUs Deleted Successfully', 'تم حذف من نحنا  بنجاح');
        }

    }
}

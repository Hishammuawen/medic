<?php

namespace App\Http\Requests\api\Assets;

use Illuminate\Foundation\Http\FormRequest;

class AssetShowRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'src' => [
                'required',
                'string'
            ],
        ];
    }
}

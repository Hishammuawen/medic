
@extends('master')
@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 style="margin:0px; padding:15px;" class="page-header">إدارة الموقع الإلكتروني</h1><!-- /.col-lg-12 -->
            </div>

            <div class="row">
                <div class="modal-body">
                    <form action="{{asset(url(env('CMS_NAME').'/save-about'))}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label"> اسم الشركة</label>
                                @if(isset($about->title))
                                   <input type="text" class="form-control" id="recipient-name" value="{{$about->title}}" name="title" required>
                           @else
                                    <input type="text" class="form-control" id="recipient-name" value="" name="title" required>

                                @endif
                            </div>
                        </div>
<!-- hello i am tareq -->
                        <!-- hello i am hisham -->
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">  صورة   </label>
                                @if(isset($about->title))
                                <input type="file" class="form-control" id="recipient-name" value="{{$about->title}}"name="image">
							<img height="100" width="100" src="{{asset('asset/images/'.$about->image)}}">
                        @else
                                    <input type="file" class="form-control" id="recipient-name" value=""name="image" required>

                                @endif
                            </div>
                        </div>





                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="recipient-name" class="col-form-label">  لمحة عن الشخص </label>
                                @if(isset($about->description))
                                <textarea class="form-control" rows="6" id="recipient-name" name="description" required>{{$about->description}}</textarea>
                                @else
                                    <textarea class="form-control" rows="6" id="recipient-name" name="description" required></textarea>

                                @endif
                            </div>
                        </div>


                        <div style="text-align: right" class="modal-footer">
                            <button type="submit" class="btn btn-primary">حفظ</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
@stop

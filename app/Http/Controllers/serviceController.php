<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Service;
use App\Menu;
use App\Page;
use Illuminate\Http\Request;
use Alert;
class serviceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $menus=Service::all();

        return view("dashboard.service.index",compact("menus"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        return  view("dashboard.service.addService");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // dd($request->all());
        try {
            $page = new  Service();
            $page['title'] = $request->get('title');
            $page['description'] = $request->get('description');
            if($request->file('image') != null){
                $image = $request->file('image');
                $filename = time() . '.' . $image->getClientOriginalExtension();
                $path = public_path('asset/images/');
                $image->move($path, $filename);
                $page['image'] = $filename;
            }
            $page->save();
            Alert::success('Success Message', 'تم اضافة الزبون بنجاح');
            return redirect(env("CMS_NAME")."/all-elements-service");

        }catch (\Exception $e){
            dd($e);
            Alert::error('Error Message', 'Optional Title');
            return \Redirect::back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $menus=Service::find($id);
            return view("dashboard.service.edit",compact("menus"));
        }catch (\Exception $exception){
            return  \Redirect::back();
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {
            $page =  Service::findOrFail($request->get('id'));
            $page['title'] = $request->get('title');
            $page['description'] = $request->get('description');
            if($request->file('image') != null){
                $image = $request->file('image');
                $filename = time() . '.' . $image->getClientOriginalExtension();
                $path = public_path('asset/images/');
                $image->move($path, $filename);
                $page['image'] = $filename;
            }
            $page->save();
            Alert::success('Success Message', 'تم اضافة الزبون بنجاح');
            return redirect(env("CMS_NAME")."/all-elements-service");

        }catch (\Exception $e){
            Alert::error('Error Message', 'Optional Title');
            return \Redirect::back();
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $menu = Service::find($id);
            $menu->delete();
            alert()->success('شكرا لك','تم حذف المجلد بنجاح ');
            return redirect(env("CMS_NAME")."/all-elements-service");
        } catch (\Exception $e) {
            alert()->error('شكرا لك','تم حذف العنصر ');

        }
    }
}

<?php


namespace App\Repositories\api;


use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class AssetsRepository
{
    /**
     * @param $file_name
     * @return mixed
     */
    public function getByFileName($file_name)
    {
        $path = storage_path() . '/app/' . get_assets_directory() . '/' . $file_name;

        if (!Storage::exists(get_assets_directory() . '/' . $file_name)) {
            throw new NotFoundHttpException();
        }

        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $mime = finfo_file($finfo, $path);
        finfo_close($finfo);

        // CSS files return text/plain
        $path_parts = pathinfo($path);
        if ($path_parts['extension'] === "css") {
            $mime = "text/css";
        }

        return response(file_get_contents($path), 200)->header('Content-Type', $mime);
    }
}

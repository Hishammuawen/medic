<?php
namespace App\Http\Controllers\api;

use App\Http\Controllers\ApiController;
use App\Http\Requests\api\ContactUs\contactUsDestroyRequest;
use App\Http\Requests\api\ContactUs\contactUsIndexRequest;
use App\Http\Requests\api\ContactUs\contactUsShowRequest;
use App\Http\Requests\api\ContactUs\contactUsStoreRequest;
use App\Http\Requests\api\ContactUs\contactUsUpdateRequest;
use App\Repositories\api\categoriesRepository;

class CategoryController extends ApiController
{
    protected $categories_repository;

    public function __construct(categoriesRepository $categories__repository)
    {
        parent::__construct();
        $this->categories__repository = $categories__repository;
    }

    public function index(contactUsIndexRequest $request)
    {
        return $this->respondWithPagination($this->categories_repository->searchableIndex($request->validated(),
            $this->getLimit()));

    }

    public function store(contactUsStoreRequest $request)
    {
        return $this->respond($this->categories_repository->create($request->validated()));
    }

    public function show(contactUsShowRequest $request,$id)
    {
        return $this->respond($this->categories_repository->getById($id));
    }

    public function update(contactUsUpdateRequest $request,$id)
    {
        return $this->respond($this->categories_repository->updateById($id, $request->validated()));
    }

    public function destroy(contactUsDestroyRequest $request,$id)
    {
        if ($this->categories__repository->softDeleteById($id)) {
            return $this->respondDeletedSuccessfully('categories Deleted Successfully', 'تم حذف الفئاتا  بنجاح');
        }


    }
}

<?php

namespace App\Http\Controllers;

use App\Component;
use App\Customer;
use App\Section;
use App\Page;
use Illuminate\Http\Request;
use Alert;
class publisherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $menus=Customer::where('type','=','Publisher')->get();

        return view("dashboard.publisher.index",compact("menus"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return  view("dashboard.publisher.addComponent");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $page = new Customer();
            $page['title'] = $request->get('title');
            $page['description'] = $request->get('description');
            $page['type'] ="Publisher";
            if($request->file('image') != null){
                $image = $request->file('image');
                $filename = time() . '.' . $image->getClientOriginalExtension();
                $path = public_path('asset/images/');
                $image->move($path, $filename);
                $page['image'] = $filename;
            }
            if($request->file('pdf') != null){
               // dd($request->all());

                $image = $request->file('pdf');
                $filename =  $image->getClientOriginalName();
                $path = public_path('asset/files/');
                $image->move($path, $filename);
                $page['file'] = $filename;
            }
            $page->save();
            Alert::success('Success Message', 'تم اضافة الزبون بنجاح');
            return redirect(env("CMS_NAME")."/all-publisher");

        }catch (\Exception $e){
            dd($e);
            Alert::error('Error Message', 'Optional Title');
            return \Redirect::back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        try {
            $menus=Customer::find($id);

            return view("dashboard.publisher.edit",compact("menus"));
        }catch (\Exception $exception){
            return  \Redirect::back();
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {
            $page = Customer::findOrfail($request->get('id'));
            $page['title'] = $request->get('title');
            $page['description'] = $request->get('description');
            $page['type'] ="Publisher";
            if($request->file('image') != null){
                $image = $request->file('image');
                $filename = time() . '.' . $image->getClientOriginalExtension();
                $path = public_path('asset/images/');
                $image->move($path, $filename);
                $page['image'] = $filename;
            }
            if($request->file('pdf') != null){
                // dd($request->all());

                $image = $request->file('pdf');
                $filename =  $image->getClientOriginalName();
                $path = public_path('asset/files/');
                $image->move($path, $filename);
                $page['file'] = $filename;
            }
            $page->save();
            Alert::success('Success Message', 'تم اضافة الزبون بنجاح');
            return redirect(env("CMS_NAME")."/all-publisher");

        }catch (\Exception $e){
            dd($e);
            Alert::error('Error Message', 'Optional Title');
            return \Redirect::back();
        }
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $menu = Customer::find($id);
            $menu->delete();
            alert()->success('شكرا لك','تم حذف العنصر ');
            return redirect(env("CMS_NAME")."/all-publisher");
        } catch (\Exception $e) {
            alert()->success(' حدث خطأ','لم يتم حذف العنصر ');

        }
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Event;
use Illuminate\Http\Request;
use DB;

class EventsController extends Controller
{
    public function index()
    {
        $all=DB::table('events')->get();
        return view('Dashbord/events',compact('all'));
    }

    public function create()
    {
        return view('Dashbord/addevent');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'title' => 'required',
            'shortcut_news' => 'required',
            'description' => 'required',
            'date' => 'required'
        ]);

        DB::table('events')->insert(
            ['name' =>$request->name ,
                'title' =>$request->title ,
                'shortcut_news' =>$request->shortcut_news ,
                'description' => $request->description,
                'date' => $request->date
            ]
        );
        //  dd($a);

        return redirect('allevents');
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $event = Event::findOrFail($id);
        // dd();
        return view('Dashbord/editevent',compact('event'));
    }

    public function update(Request $request, $id)
    {
        $event = Event::findOrFail($id);
        $event->name=$request['name'];
        $event->title=$request['title'];
        $event->shortcut_news=$request['shortcut_news'];
        $event->description=$request['description'];
        $event->date=$request['date'];
        $event->save();

        return redirect('allevents');
    }

    public function destroy($id)
    {
        DB::table('events')->where('id', '=',$id)->delete();

        return redirect('allevents');
    }
}

<?php

namespace App\Http\Controllers;

use App\Component;
use App\Section;
use App\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class siteController extends Controller
{

    /**
     * @param $id
     * @param $title
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public  function load ($id, $title){
    $component=Section::where('menu_id','=',$id)->get();
     return view("site.blog.home",compact('component','title'));
    }
    public function loadSection($id , $title){
        $component=Component::where('sec_id','=',$id)->get();
        return view("site.blog.homeComponent",compact('component','title'));
    }
    public function loadDetail($id, $title){
        $comp=Component::where('id','=',$id)->get();
        $component=Page::where('id','=',$comp[0]->page_id)->get();
//        dd($component);
        return view("site.blog.detail",compact('component','comp'));
    }
}

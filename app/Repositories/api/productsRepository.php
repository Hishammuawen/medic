<?php


namespace App\Repositories\api;

use App\Exceptions\AppException;
use App\Models\Product;
use App\Repositories\BaseRepository;
use DB;
use Exception;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class productsRepository extends BaseRepository
{
    public function model()
    {
        return Product::class;
    }

    /**
     * @inheritDoc
     */
    public function relations()
    {
        return [
            'category'
        ];
    }

    public function create(array $data)
    {
        DB::beginTransaction();


        $products = parent::create($data);

        if (!empty($data['category'] ?? [])) {
            $products->roles()->sync($data['category']);
        }

        DB::commit();
        return $products;
    }

    public function updateById($id, array $data, array $options = [])
    {
        DB::beginTransaction();


        $products = parent::updateById($id, $data);

        if (!empty($data['category'] ?? [])) {
            $products->roles()->sync($data['category']);
        }

        DB::commit();
        return $products->refresh();
    }
}

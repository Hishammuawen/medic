<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\ApiController;
use App\Http\Requests\api\News\newsDestroyRequest;
use App\Http\Requests\api\News\newsIndexRequest;
use App\Http\Requests\api\News\newsShowRequest;
use App\Http\Requests\api\News\newsStoreRequest;
use App\Http\Requests\api\News\newsUpdateRequest;
use App\Repositories\api\newsRepository;

class NewsController extends ApiController
{
    protected $news_repository;

    public function __construct(newsRepository $news_repository)
    {
        parent::__construct();
        $this->news_repository = $news_repository;
    }

    public function index(newsIndexRequest $request)
    {
        return $this->respondWithPagination($this->news_repository->searchableIndex($request->validated(),
            $this->getLimit()));

    }

    public function store(newsStoreRequest $request)
    {
        return $this->respond($this->news_repository->create($request->validated()));
    }

    public function show(newsShowRequest $request, $id)
    {
        return $this->respond($this->news_repository->getById($id));
    }

    public function update(newsUpdateRequest $request,$id)
    {
        return $this->respond($this->news_repository->updateById($id, $request->validated()));
    }

    public function destroy(newsDestroyRequest $request,$id)
    {
        if ($this->news_repository->softDeleteById($id)) {
            return $this->respondDeletedSuccessfully('news Deleted Successfully', 'تم حذف الأخبارا  بنجاح');
        }

    }
}

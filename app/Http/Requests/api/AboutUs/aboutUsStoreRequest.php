<?php

namespace App\Http\Requests\api\AboutUs;

use Illuminate\Foundation\Http\FormRequest;

class aboutUsStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can('store_aboutUs');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => [
                'nullable',
                'string'
            ],
            'description' => [
                'nullable',
                'string'
            ],
            'objectives' => [
                'nullable',
                'string'
            ],
            'vision' => [
                'nullable',
                'string'
            ],

        ];
    }
}

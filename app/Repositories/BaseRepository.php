<?php /** @noinspection PhpUnused */

namespace App\Repositories;

use App\Exceptions\AppException;
use App\Http\Traits\SearchableQueryBuilder;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

/**
 * Class BaseRepository.
 */
abstract class BaseRepository implements RepositoryContract
{
    use SearchableQueryBuilder;
    /**
     * The repository model.
     *
     * @var Model
     */
    protected $model;

    /**
     * The query builder.
     *
     * @var Builder
     */
    protected $query;

    /**
     * Alias for the query limit.
     *
     * @var int
     */
    protected $take;

    /**
     * Array of related models to eager load.
     *
     * @var array
     */
    protected $with = [];

    /**
     * Array of one or more where clause parameters.
     *
     * @var array
     */
    protected $wheres = [];

    /**
     * Array of one or more where in clause parameters.
     *
     * @var array
     */
    protected $whereIns = [];

    /**
     * Array of one or more ORDER BY column/value pairs.
     *
     * @var array
     */
    protected $orderBys = [];

    /**
     * Array of scope methods to call on the model.
     *
     * @var array
     */
    protected $scopes = [];

    /**
     * Array of relations of the model.
     *
     * @var array
     */
    protected $relations = [];

    /**
     * BaseRepository constructor.
     * @throws AppException
     * @throws BindingResolutionException
     */
    public function __construct()
    {
        $this->makeModel();
        $this->setRelations();
    }

    /**
     * @return Model|mixed
     * @throws AppException
     * @throws BindingResolutionException
     */
    public function makeModel()
    {
        $model = app()->make($this->model());

        if ($model) {
            if (!$model instanceof Model) {
                throw new AppException('general.create_model');
            }

            return $this->model = $model;

        }

        return null;
    }

    /**
     * Specify Model class name.
     *
     * @return mixed
     */
    abstract public function model();

    /**
     *
     */
    public function setRelations()
    {
        $this->relations = $this->relations();
    }

    /**
     * Specify Model relations.
     *
     * @return array
     */
    abstract public function relations();

    /**
     * Get all the model records in the database.
     *
     * @param  array  $columns
     *
     * @return Builder[]|Collection
     */
    public function all(array $columns = ['*'])
    {
        $this->newQuery()->eagerLoad();

        $models = $this->query->get($columns);

        $this->unsetClauses();

        return $models;
    }

    /**
     * Add relationships to the query builder to eager load.
     *
     * @return $this
     */
    protected function eagerLoad()
    {
        foreach ($this->with as $relation) {
            $this->query->with($relation);
        }

        return $this;
    }

    /**
     * Create a new instance of the model's query builder.
     *
     * @return $this
     */
    protected function newQuery()
    {
        $this->query = $this->model->newQuery();

        return $this;
    }

    /**
     * Reset the query clause parameter arrays.
     *
     * @return $this
     */
    protected function unsetClauses()
    {
        $this->wheres = [];
        $this->whereIns = [];
        $this->scopes = [];
        $this->take = null;

        return $this;
    }

    /**
     * Count the number of specified model records in the database.
     *
     * @return int
     */
    public function count(): int
    {
        return $this->get()->count();
    }

    /**
     * Get all the specified model records in the database.
     *
     * @param  array  $columns
     *
     * @return Builder[]|Collection
     */
    public function get(array $columns = ['*'])
    {
        $this->newQuery()->eagerLoad()->setClauses()->setScopes();

        $models = $this->query->get($columns);

        $this->unsetClauses();

        return $models;
    }

    /**
     * Set query scopes.
     *
     * @return $this
     */
    protected function setScopes()
    {
        foreach ($this->scopes as $method => $args) {
            $this->query->$method(implode(', ', $args));
        }

        return $this;
    }

    /**
     * Set clauses on the query builder.
     *
     * @return $this
     */
    protected function setClauses()
    {
        foreach ($this->wheres as $where) {
            $this->query->where($where['column'], $where['operator'], $where['value']);
        }

        foreach ($this->whereIns as $whereIn) {
            $this->query->whereIn($whereIn['column'], $whereIn['values']);
        }

        foreach ($this->orderBys as $orders) {
            $this->query->orderBy($orders['column'], $orders['direction']);
        }

        if (isset($this->take) and !is_null($this->take)) {
            $this->query->take($this->take);
        }

        return $this;
    }

    /**
     * Delete one or more model records from the database.
     *
     * @return mixed
     */
    public function delete()
    {
        $this->newQuery()->setClauses()->setScopes();

        $result = $this->query->delete();

        $this->unsetClauses();

        return $result;
    }

    /**
     * Delete the specified model record from the database.
     *
     * @param $id
     *
     * @return bool|null
     * @throws Exception
     */
    public function deleteById($id): bool
    {
        $this->unsetClauses();

        return $this->getById($id)->delete();
    }

    /**
     * Get the specified model record from the database.
     *
     * @param       $id
     * @param  array  $columns
     *
     * @return Model
     */
    public function getById($id, array $columns = ['*'])
    {
        $this->unsetClauses();

        $this->newQuery()->eagerLoad();

        /** @var Model $model */
        $model = $this->query->with($this->relations)
            ->findOrFail($id, $columns);

        return $model;
    }

    /**
     * Delete multiple records.
     *
     * @param  array  $ids
     *
     * @return int
     */
    public function deleteMultipleById(array $ids): int
    {
        return $this->model->destroy($ids);
    }

    /**
     * Get the first specified model record from the database.
     *
     * @param  array  $columns
     *
     * @return Builder|Model
     */
    public function first(array $columns = ['*'])
    {
        $this->newQuery()->eagerLoad()->setClauses()->setScopes();

        $model = $this->query->firstOrFail($columns);

        $this->unsetClauses();

        return $model;
    }

    /**
     * @param       $item
     * @param       $column
     * @param  array  $columns
     *
     * @return Builder|Model|object|null
     */
    public function getByColumn($item, $column, array $columns = ['*'])
    {
        $this->unsetClauses();

        $this->newQuery()->eagerLoad();

        return $this->query->where($column, $item)->first($columns);
    }

    /**
     * @param  int  $limit
     * @param  array  $columns
     * @param  string  $pageName
     * @param  null  $page
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginate($limit = 25, array $columns = ['*'], $pageName = 'page', $page = null)
    {
        $this->newQuery()->eagerLoad()->setClauses()->setScopes();

        $models = $this->query->paginate($limit, $columns, $pageName, $page);

        $this->unsetClauses();

        return $models;
    }

    /**
     * @param  array  $data
     * @param  int  $paged
     * @param  null  $orderBy
     * @param  null  $sort
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function searchableIndex(array $data = [], $paged = 0, $orderBy = null, $sort = null)
    {
        $relations = [];

        $query = $this->indexQuery();

        return $this->getSearchablePaginated($query, $data, $paged, $orderBy, $sort, $relations);
    }

    /**
     * @param  array  $relations
     * @return Builder
     */
    public function indexQuery(array $relations = null)
    {
        return $this->model->newQuery()
            ->with($relations ?? $this->relations)
            ->whereNull('deleted_at');
    }

    /**
     * @param $query
     * @param  array  $searchable
     * @param $paged
     * @param $orderBy
     * @param $sort
     * @param  array  $relations
     * @param  array  $hidden
     * @param $mapCallback
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getSearchablePaginated(
        $query,
        array $searchable,
        $paged,
        $orderBy,
        $sort,
        $relations = [],
        $hidden = [],
        $mapCallback = null
    ) {
        $result = $this->withEncryption()
            ->useQuery($query)
            ->useModel($this->model)
            ->searchModelProperties($searchable)
            ->useRelations(empty($relations) ? $this->relations : $relations)
            ->searchModelRelationsProperties($searchable)
            ->sortModel($orderBy, $sort)
            ->buildQuery()
            ->get();

        return $this->reformatAndPaginate($result, $hidden, $mapCallback, $paged);
    }

    /**
     * @param  \Illuminate\Support\Collection  $collection
     * @param  array  $hidden
     * @param $mapCallback
     * @param $paged
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function reformatAndPaginate(\Illuminate\Support\Collection $collection, array $hidden, $mapCallback, $paged)
    {
        /** @var Collection $collection */
        $collection = $collection->makeHidden($hidden);

        if ($mapCallback) {
            $collection = $collection->map($mapCallback);
        }

        return $this->paginateCollection($collection, $paged);
    }

    /**
     * @param  \Illuminate\Support\Collection  $collection
     * @param $paged
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function paginateCollection(\Illuminate\Support\Collection $collection, $paged)
    {
        $paged = $paged === 0 ? $collection->count() : $paged;

        $current_page = LengthAwarePaginator::resolveCurrentPage();

        $current_page_collection = $collection->slice(($current_page * $paged) - $paged, $paged)->all();

        return new LengthAwarePaginator($current_page_collection, count($collection), $paged === 0 ? 10 : $paged);
    }

    /**
     * @return Builder
     */
    public function indexQueryWithDeleted()
    {
        return $this->model->newQuery()
            ->with($relations ?? $this->relations);
    }

    /**
     * @param  Model  $model
     * @param  array  $data
     * @return Model
     */
    public function update(Model $model, array $data){
        $this->unsetClauses();
        $model->update($data);
        return $model;
    }

    /**
     * Set the query limit.
     *
     * @param  int  $limit
     *
     * @return $this
     */
    public function limit($limit)
    {
        $this->take = $limit;

        return $this;
    }

    /**
     * Set an ORDER BY clause.
     *
     * @param  string  $column
     * @param  string  $direction
     * @return $this
     */
    public function orderBy($column, $direction = 'asc') {
        $this->orderBys[] = compact('column', 'direction');
        return $this;
    }

    /**
     * Add a simple where clause to the query.
     *
     * @param  string  $column
     * @param  string  $value
     * @param  string  $operator
     *
     * @return $this
     */
    public function where($column, $value, $operator = '=')    {
        $this->wheres[] = compact('column', 'value', 'operator');
        return $this;
    }

    /**
     * Add a simple where in clause to the query.
     *
     * @param  string  $column
     * @param  mixed  $values
     *
     * @return $this
     */
    public function whereIn($column, $values)
    {
        $values = is_array($values) ? $values : [$values];

        $this->whereIns[] = compact('column', 'values');

        return $this;
    }

    /**
     * Set Eloquent relationships to eager load.
     *
     * @param  array  $relations
     *
     * @return $this
     */
    public function with(array $relations)
    {
        $this->with = $relations;
        return $this;
    }

    /**
     * Create one or more new model records in the database.
     *
     * @param  array  $data
     *
     * @return Collection
     * @throws Exception
     */
    public function createMultiple(array $data)
    {
        $models = new Collection();

        foreach ($data as $d) {
            $models->push($this->create($d));
        }

        return $models;
    }

    /**
     * Create a new model record in the database.
     *
     * @param  array  $data
     *
     * @return Model
     * @throws Exception
     */
    public function create(array $data)
    {
        DB::beginTransaction();

        $this->unsetClauses();
        $model = $this->model->create($data);

        DB::commit();

        /**
         * @var Model $model
         */
        return $model->load($this->relations);
    }

    /**
     * Fill a new model record without saving it in the database
     *
     * @param  array  $data
     * @return Model
     */
    public function fill(array $data){
        $this->unsetClauses();
        return $this->model->fill($data);
    }

    /**
     * @param $id
     * @return bool
     * @throws AppException
     */
    public function restoreById($id): bool
    {
        $this->unsetClauses();
        $model = $this->getById($id);
        return $this->restore($model);
    }

    /**
     * @param  Model  $model
     * @return bool
     * @throws AppException
     */
    public function restore(Model $model): bool
    {
        $this->unsetClauses();

        if (!$model->deleted_at) {
            throw new AppException('general.already_active');
        }

        return $model->update([
            'deleted_at' => null
        ]);
    }

    /**
     * @param $query
     * @param $sort_by
     * @param $sort_direction
     * @return mixed
     */
    public function sort($query, $sort_by, $sort_direction)
    {
        $columns = Schema::getColumnListing($this->model->getTable());
        if (in_array($sort_by ?? 'created_at', $columns, true) !== false) {
            /**
             * @var \Illuminate\Database\Query\Builder $query
             */
            $query->orderBy($sort_by ?: 'created_at', $sort_direction ?: 'desc');
        }

        return $query;
    }

    /**
     * @param  FormRequest  $request
     * @return Model
     * @throws Exception
     */
    public function createUsingRequest(FormRequest $request)
    {
        return $this->create($request->validated());
    }

    /**
     * @param  FormRequest  $request
     * @param $id
     * @return Collection|Model
     * @throws Exception
     */
    public function updateUsingRequest(FormRequest $request, $id)
    {
        return $this->updateById($request->validated(), $id);
    }

    /**
     * Update the specified model record in the database.
     *
     * @param       $id
     * @param  array  $data
     * @param  array  $options
     * @return Collection|Model
     * @throws Exception
     */
    public function updateById($id, array $data, array $options = [])
    {
        $this->unsetClauses();

        $model = $this->getById($id);

        DB::beginTransaction();

        $model->update($data, $options);

        DB::commit();
        return $model;
    }

    /**
     * @param $id
     * @return bool
     * @throws AppException
     */
    public function softDeleteById($id): bool
    {
        $model = $this->getById($id);

        return self::softDelete($model);
    }

    /**
     * @param  Model  $model
     * @return bool
     * @throws AppException
     */
    public function softDelete(Model $model): bool
    {
        $this->unsetClauses();

        if ($model->deleted_at) {
            throw new AppException('general.already_deleted');
        }

        return $model->update([
            'deleted_at' => Carbon::now()
        ]);
    }

    /**
     * @param  Builder  $query
     * @param $start_date
     * @param $end_date
     * @param  string  $date_field
     * @return mixed
     */
    public function filterWithinDate(Builder $query, $start_date, $end_date, $date_field = 'created_at')
    {
        return $this->filterWithinRange(
            $query,
            $date_field,
            $start_date ? Carbon::parse($start_date)->toDateTimeString() : null,
            $end_date ? Carbon::parse($end_date)->addDay()->toDateTimeString() : null,
            true,
            false
        );
    }

    /**
     * @param  Builder  $query
     * @param  string  $field
     * @param $start_value
     * @param $end_value
     * @param  bool  $or_equal_start
     * @param  bool  $or_equal_end
     * @return Builder
     */
    public function filterWithinRange(
        Builder $query,
        string $field,
        $start_value,
        $end_value,
        $or_equal_start = true,
        $or_equal_end = true
    ) {
        if ($start_value !== null) {
            $query = $query->where($field, $or_equal_start ? '>=' : '>', $start_value);
        }

        if ($end_value !== null) {
            $query = $query->where($field, $or_equal_end ? '<=' : '<', $end_value);
        }

        return $query;
    }

    /**
     * @param  array  $data
     * @return Builder[]|Collection
     */
    public function getExportToExcelData(array $data)
    {
        $headers = $this->getAttributesAndRelations(array_map(function ($header) {
            return json_decode($header)->name;
        }, $data['headers'] ?? []));

        return $this->indexQuery($headers['relations'])
            ->get()
            ->makeHidden(array_merge(array_diff($headers['all'], $headers['attributes']), ['id']))
            ->values();
    }

    /**
     * @param $headers
     * @return array
     */
    protected function getAttributesAndRelations($headers)
    {
        $fillable = $this->model->getFillable();
        $appended = $this->model->getMutatedAttributes();
        $all = array_merge(array_values($fillable), array_values($appended));

        $attributes = array_intersect($all, $headers);
        $relations = array_diff($headers, $attributes);

        return [
            'attributes' => $attributes,
            'relations' => $relations,
            'all' => $all
        ];
    }

    /**
     * @param  Builder  $query
     * @param $term
     * @param  array  $attributes
     * @return Builder|Builder
     */
    public function searchByQueryString(Builder $query, $term, array $attributes)
    {
        return $query->where(function (Builder $query) use ($term, $attributes) {
            foreach ($attributes as $attribute) {
                $query->orWhere($attribute, 'LIKE', '%'.$term.'%');
            }
        });
    }
}


@extends('master',['title'=>"أضافة مادة جديد"])
@section('content')

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
<h1 style="margin:0px; padding:15px;" class="page-header">إدارة الموقع الإلكتروني</h1><!-- /.col-lg-12 -->
        </div>
        <div class="modal-body">
            <p style="font-size: 150%">  إضافة زبون</p>

            <form action="{{asset(url(env('CMS_NAME').'/save-items'))}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="col-lg-3">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">اسم المادة</label>
                        <input type="text" class="form-control" id="recipient-name" name="title" required>
                    </div>
                </div>
				
                <div class="col-lg-3">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label"> الصورة</label>
                        <input type="file" class="form-control" id="recipient-name" name="image" required>
                    </div>
                </div>
				 <div class="col-lg-3">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label"> سعر المادة</label>
                        <input type="number" class="form-control" id="recipient-name" name="price" required>
                    </div>
                </div>
				<div class="col-lg-3">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">الكون </label>
							<select class="form-control" name="component_id">
							  @foreach($Component as $f)
							     <option value="{{$f->id}}">{{$f->title}}</option>
							  @endforeach
							</select>
						</div>
                </div>
                <div class="form-group">
                    <label for="recipient-name" class="col-form-label">شرح عن المادة </label>
                    <div class="adjoined-bottom">
                        <div class="grid-container">
                            <div class="grid-width-100">
				<textarea id="editor" name="description">
			</textarea>
                            </div>
                        </div>
                    </div>                </div>
                <div style="text-align: right" class="modal-footer">
                    <button type="submit" class="btn btn-primary">حفظ</button>
                </div>
            </form>
        </div>
    </div>
        <script>
            initSample();
        </script>
@stop

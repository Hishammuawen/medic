<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\ApiController;
use App\Http\Requests\api\Assets\AssetShowRequest;
use App\Repositories\api\AssetsRepository;

class AssetController extends ApiController
{
    protected $assets_repository;

    /**
     * AssetController constructor.
     * @param $assets_repository
     */
    public function __construct(AssetsRepository $assets_repository)
    {
        parent::__construct();
        $this->assets_repository = $assets_repository;
    }

    /**
     * @api {get} assets  Request Specific File By Name.
     * @apiName Show Asset
     * @apiGroup Assets
     * @apiuse AssetShowParams
     * @apiSuccess {file} file  Requested File.
     * @apiError (Error 404)    NotFound   When the asset is not found
     * @apiuse NotFoundErrorExample
     */
    public function show(AssetShowRequest $request, $name)
    {
        return $this->assets_repository->getByFileName($name);
    }
}

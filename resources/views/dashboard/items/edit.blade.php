
@extends('master',['title'=>"تعديل السجلات"])
@section('content')

    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
<h1 style="margin:0px; padding:15px;" class="page-header">إدارة الموقع الإلكتروني</h1><!-- /.col-lg-12 -->
        </div>
        <div class="modal-body">
            <p style="font-size: 150%">عنوان الصفحة</p>

            <form action="{{asset(url(env('CMS_NAME').'/update-items'))}}" method="post" enctype="multipart/form-data">
                @csrf
				                <div class="col-lg-3">

                     <div class="form-group">
                        <label for="recipient-name" class="col-form-label">اسم المادة</label>
                        <input type="text" class="form-control" value="{{$page->title}}" id="recipient-name" name="title" required>
                    </div>
                </div>
				
                <div class="col-lg-3">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label"> الصورة</label>
                        <input type="file" class="form-control" id="recipient-name" name="image" >
						<img src="{{asset('asset/images/'.$page->image)}}" height="100" width="100">
				   </div>
                </div>
				 <div class="col-lg-3">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label"> سعر المادة</label>
                        <input type="number" value="{{$page->price}}" class="form-control" id="recipient-name" name="price" required>
                    </div>
                </div>
				<div class="col-lg-3">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">الكون </label>
							<select class="form-control" name="component_id">
							  @foreach($pages as $f)
							   @if($f->id == $page->component_id)
							     <option value="{{$f->id}}" selected>{{$f->title}}</option>
							 @else
									<option value="{{$f->id}}" >{{$f->title}}</option>
							 @endif
							  @endforeach
							</select>
						</div>
                </div>
                <div class="col-lg-12">
                    <div class="adjoined-bottom">
                        <div class="grid-container">
                            <div class="grid-width-100">
                                <textarea id="editor" name="description">
                                    {{$page->description}}
                                </textarea>
                            </div>
                        </div>
                    </div>

                </div>
                <div style="text-align: right" class="modal-footer">
                    <button type="submit" class="btn btn-primary">حفظ</button>
                </div>
                <div class="form-group">
                    <input required type="hidden" class="form-control" id="recipient-name" name="id" value="{{$page->id}}">
                </div>
            </form>
        </div>
    </div>
        <script>
            initSample();
        </script>
@stop

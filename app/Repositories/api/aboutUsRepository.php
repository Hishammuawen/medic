<?php


namespace App\Repositories\api;


use App\Exceptions\AppException;
use App\Models\AboutUs;
use App\Repositories\BaseRepository;
use DB;
use Exception;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;


class aboutUsRepository extends BaseRepository
{
    public function model()
    {
        return AboutUs::class;
    }


    public function relations()
    {
        return [
            //
        ];
    }


    public function create(array $data)
    {

        $aboutUs = parent::create($data);

        return $aboutUs;
    }

    public function updateById($id, array $data, array $options = [])
    {
        $aboutUs = parent::updateById($id, $data);
        return $aboutUs->refresh();
    }

}

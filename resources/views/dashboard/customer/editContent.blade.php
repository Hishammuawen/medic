
@extends('master',['title'=>"تعديل السجلات"])
@section('content')
    <script src="{{asset('ckeditor/ckeditor.js')}}"></script>
    <script src="{{asset('ckeditor/samples/js/sample.js') }}"></script>
    <link rel="stylesheet" href="{{asset('ckeditor/samples/css/samples.css')}}">
    <link rel="stylesheet" href="{{asset('ckeditor/samples/toolbarconfigurator/lib/codemirror/neo.css')}}">
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
<h1 style="margin:0px; padding:15px;" class="page-header">إدارة الموقع الإلكتروني</h1><!-- /.col-lg-12 -->
        </div>
        <div class="modal-body">
            <p style="font-size: 150%">{{$page->title}}</p>

            <form action="{{asset(url(env('CMS_NAME').'/update-content'))}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">اضافة صورة</label>
                        <input type="file" class="form-control" id="image" name="image"  >
                        <img  name="hisham" height="100" src="{{asset('/asset/images/'.$page->image)}}">

                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">

                        <input type="hidden" class="form-control" id="recipient-name" name="id" value="{{$page->id}}" required>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="adjoined-bottom">
                        <div class="grid-container">
                            <div class="grid-width-100">
				<textarea id="editor" name="content">
                    {{$page->content}}
			</textarea>
                            </div>
                        </div>
                    </div>

                </div>

                <div style="text-align: right" class="modal-footer">
                    <br>
                    <button type="submit" class="btn btn-primary">حفظ</button>
                </div>

            </form>
        </div>
    </div>

        <script>
            initSample();
        </script>
@stop

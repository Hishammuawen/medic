
@extends('master',['title'=>"أضافة خدمة جديد"])
@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 style="margin:0px; padding:15px;" class="page-header">إدارة الموقع الإلكتروني</h1><!-- /.col-lg-12 -->
            </div>
            <div class="modal-body">
                <p style="font-size: 150%">  إضافة مكون جديد</p>

                <form action="{{asset(url(env('CMS_NAME').'/save-component'))}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">اسم المكون</label>
                            <input type="text" class="form-control" id="recipient-name" name="title" required>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">اسم المكون</label>
                            <select name="service_id" class="form-control">
                                @foreach($services as $s)
                                     <option value="{{$s->id}}"> {{$s->title}}</option>
                                    @endforeach
                            </select>
                        </div>
                    </div>
                    <div style="text-align: right" class="modal-footer">
                        <button type="submit" class="btn btn-primary">حفظ</button>
                    </div>
                </form>
            </div>
        </div>


@stop

<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['api', 'auth.basic'])->namespace('api')->group(function () {
    // Users
    Route::apiResource('users', 'UserController');
    Route::apiResource('contactUs', 'ContactUsController');
    Route::apiResource('abouttUs', 'AboutUsController');
    Route::apiResource('news', 'NewsController');
    Route::apiResource('events', 'EventController');
    Route::apiResource('products', 'ProductController');
    Route::apiResource('categories', 'CategoryController');
});

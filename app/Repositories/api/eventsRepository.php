<?php


namespace App\Repositories\api;

use App\Exceptions\AppException;
use App\Models\Event;
use App\Repositories\BaseRepository;
use DB;
use Exception;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;


class eventsRepository extends BaseRepository
{
    public function model()
    {
        return Event::class;
    }


    public function relations()
    {
        return [
            //
        ];
    }


    public function create(array $data)
    {

        $events = parent::create($data);

        return $events;
    }

    public function updateById($id, array $data, array $options = [])
    {
        $events = parent::updateById($id, $data);
        return $events->refresh();
    }

}

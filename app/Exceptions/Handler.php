<?php

namespace App\Exceptions;

use App\Http\Traits\RestfulRespond;
use BadMethodCallException;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Symfony\Component\CssSelector\Exception\InternalErrorException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

class Handler extends ExceptionHandler
{
    use RestfulRespond;
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param Exception $exception
     *
     * @return void
     * @throws Exception
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param Request $request
     * @param Exception $exception
     *
     * @return Response
     */
    public function render($request, Exception $exception)
    {
dd($exception);
        if ($exception instanceof NotFoundHttpException) {

                        return response()->view('error.error',['status'=>$exception->getMessage(). 'Not Found Htt pException ','path'=>$request->decodedPath(),'code'=>$exception->getStatusCode()], $exception->getStatusCode());
        }

        if ($exception instanceof ModelNotFoundException) {
                        return response()->view('error.error',['status'=>$exception->getMessage().'Model Not Found Exception   ','path'=>$request->decodedPath(),'code'=>$exception->getStatusCode()], $exception->getStatusCode());

        }

        if ($exception instanceof BadMethodCallException) {
                        return response()->view('error.error',['status'=>$exception->getMessage().'Bad Method Call Exception   ','path'=>$request->decodedPath(),'code'=>$exception->getStatusCode()],  $exception->getStatusCode());
        }

        if ($exception instanceof InternalErrorException) {
            return response()->view('error.error',['status'=>$exception->getMessage().'Internal Error Exception  ','path'=>$request->decodedPath(),'code'=>$exception->getStatusCode()],  $exception->getStatusCode());
        }

        if ($exception instanceof ValidationException) {
            return response()->view('error.error',['status'=>$exception->getMessage().'Validation Exception','path'=>$request->decodedPath(),'code'=>$exception->getStatusCode()],  $exception->getStatusCode());
        }

        if ($exception instanceof BadRequestHttpException) {
            return response()->view('error.error',['status'=>$exception->getMessage().'Bad Request Http Exception','path'=>$request->decodedPath(),'code'=>$exception->getStatusCode()],  $exception->getStatusCode());
        }

        if ($exception instanceof AppException) {
            return response()->view('error.error',['status'=>$exception->getMessage().'App Exception','path'=>$request->decodedPath(),'code'=>$exception->getStatusCode()],  $exception->getStatusCode());
        }

        if ($exception instanceof MethodNotAllowedHttpException) {
            return response()->view('error.error',['status'=>$exception->getMessage().' Method Not Allowed Http Exception','path'=>$request->decodedPath(),'code'=>$exception->getStatusCode()],  $exception->getStatusCode());
        }

        if ($exception instanceof AuthorizationException) {
            return response()->view('error.error',['status'=>$exception->getMessage().'Authorization Exception','path'=>$request->decodedPath(),'code'=>$exception->getStatusCode()],  $exception->getStatusCode());
        }

        if ($exception instanceof UnauthorizedHttpException) {
            return response()->view('error.error',['status'=>$exception->getMessage().'Unauthorized Http Exception','path'=>$request->decodedPath(),'code'=>$exception->getStatusCode()],  $exception->getStatusCode());
        }
        if ($exception instanceof QueryException) {
                return response()->view('error.error',['status'=>$exception->getMessage().'Query Exception ','path'=>$request->decodedPath(),'code'=>1007],  500);
            }
        if ($exception instanceof Exception) {
            return response()->view('error.error',['status'=>$exception->getMessage().'Query Exception ','path'=>$request->decodedPath(),'code'=>500],  500);
        }
        if (env('APP_ENV') !== 'local') {
            return $this->respondInternalError();
        }
        return parent::render($request, $exception);
    }
}

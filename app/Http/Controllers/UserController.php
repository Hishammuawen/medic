<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use App\Models\RoleUser;
use Illuminate\Http\Request;
use DB;

class UserController extends Controller
{

    public function index()
    {
	
        $all=DB::table('users')->get();
        return view('dashboard.user.users',compact('all'));
    }


    public function create(){
        $role=Role::all();
        return view('dashboard.user.adduser',compact('role'));
    }

    public function saveLogin(Request $request){
   //     dd(encrypt($request->get("password")));
        $result=User::where("email",'=',$request->get("email"));
      //  dd($result->get());
        if ($result->get()->isEmpty()) {
            return \Redirect::back();
        }
$user=$result->get();
        if (!$user[0]) {
            return \Redirect::back();
        }


        return redirect(env("CMS_NAME")."/all-elements-menu");

        dd(\Hash::check('INPUT PASSWORD', $user[0]->password));


    }
    public function store(Request $request)
    {
    $user=new User();
      $user["name"]=$request->get("name");
      $user["email"]=$request->get("email");
      $user["password"]=$request->get("password");

$user->save();
 $role= new RoleUser();
  $role["user_id"]=$user->id;
  $role["role_id"]=$request->get("role");
$role->save();

        return redirect(env("CMS_NAME")."/all-users");


    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $user = User::findOrFail($id);
        $role = Role::all();
        // dd();
        return view('dashboard.user.edituser',compact('user','role'));
    }
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user["name"]=$request->get("name");
        $user["email"]=$request->get("email");
        $user["password"]=$request->get("password");
        $user->save();
        $role=  RoleUser::where("user_id",'=',$user->id)->get();
        $role[0]->user_id=$user->id;
        $role[0]->role_id=$request->get("role");
        $user->save();
        return redirect(env("CMS_NAME")."/all-users");
    }

    public function destroy($id)
    {
		
$user=User::findOrFail($id);
 $role=  RoleUser::where("user_id",'=',$id)->get();
foreach($role as $r)
{
	$r->delete();
}

$user->delete();
dd($user);


        return redirect(env("CMS_NAME")."/all-users");
    }
}

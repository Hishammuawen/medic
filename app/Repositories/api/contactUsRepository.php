<?php


namespace App\Repositories\api;



use App\Exceptions\AppException;
use App\Models\ContactUs;
use App\Repositories\BaseRepository;
use DB;
use Exception;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;


class contactUsRepository extends BaseRepository
{
    public function model()
    {
        return ContactUs::class;
    }


    public function relations()
    {
        return [
            //
        ];
    }


    public function create(array $data)
    {

        $contactUs = parent::create($data);

        return $contactUs;
    }

    public function updateById($id, array $data, array $options = [])
    {
        $contactUs = parent::updateById($id, $data);
        return $contactUs->refresh();
    }

}




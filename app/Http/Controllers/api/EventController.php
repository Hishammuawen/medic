<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\ApiController;
use App\Http\Requests\api\Events\eventsDestroyRequest;
use App\Http\Requests\api\Events\eventsIndexRequest;
use App\Http\Requests\api\Events\eventsShowRequest;
use App\Http\Requests\api\Events\eventsStoreRequest;
use App\Http\Requests\api\Events\eventsUpdateRequest;
use App\Repositories\api\eventsRepository;

class EventController extends ApiController
{
    protected $events_repository;

    public function __construct(eventsRepository $events_repository)
    {
        parent::__construct();
        $this->events_repository = $events_repository;
    }

    public function index(eventsIndexRequest $request)
    {
        return $this->respondWithPagination($this->events_repository->searchableIndex($request->validated(),
            $this->getLimit()));

    }

    public function store(eventsStoreRequest $request)
    {
        return $this->respond($this->events_repository->create($request->validated()));
    }
    public function show(eventsShowRequest $request,$id)
    {
        return $this->respond($this->events_repository->getById($id));

    }


    public function update(eventsUpdateRequest $request,$id)
    {
        return $this->respond($this->events_repository->updateById($id, $request->validated()));
    }

    public function destroy(eventsDestroyRequest $request,$id)
    {
        if ($this->events_repository->softDeleteById($id)) {
            return $this->respondDeletedSuccessfully('events Deleted Successfully', 'تم حذف الفعالياتا  بنجاح');
        }

    }
}

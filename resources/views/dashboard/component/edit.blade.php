
@extends('master',['title'=>"تعديل السجلات"])
@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 style="margin:0px; padding:15px;" class="page-header">إدارة الموقع الإلكتروني</h1><!-- /.col-lg-12 -->
            </div>
            <div class="modal-body">
                <p style="font-size: 150%"> تعديل معلومات الخدمة</p>

                <form action="{{asset(url(env('CMS_NAME').'/update-component'))}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">اسم المكون</label>
                            <input type="text" class="form-control" id="recipient-name" name="title" value="{{$pages->title}}">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group">
                            <label for="recipient-name" class="col-form-label">اسم المكون</label>
                            <select name="service_id" class="form-control">
                                @foreach($services as $s)
                                    @if( $s->id == $pages->service_id)
                                    <option value="{{$s->id}}" selected> {{$s->title}}</option>
                                    @else
                                        <option value="{{$s->id}}" > {{$s->title}}</option>
                                    @endif

                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div style="text-align: right" class="modal-footer">
                        <button type="submit" class="btn btn-primary">حفظ</button>
                    </div>
                    <div class="form-group">
                        <input required type="hidden" class="form-control" id="recipient-name" name="id" value="{{$pages->id}}">
                    </div>
                </form>
            </div>
        </div>
        <script>
            initSample();
        </script>
@stop

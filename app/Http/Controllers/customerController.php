<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;
use Alert;
use Illuminate\Support\Facades\Redirect;

class customerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(){
        $pages=Customer::where('type','=','Customer')->get();
        return view("dashboard.customer.index",compact("pages"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        return  view("dashboard.customer.addCustomer");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    // dd($request->file('image'));
        try {
            $page = new Customer();
            $page['title'] = $request->get('title');
            $page['description'] = $request->get('description');
            $page['type'] ="Customer";
            if($request->file('image') != null){
                $image = $request->file('image');
                $filename = time() . '.' . $image->getClientOriginalExtension();
                $path = public_path('asset/images/');
                $image->move($path, $filename);
                $page['image'] = $filename;
            }
            $page->save();
            Alert::success('Success Message', 'تم اضافة الزبون بنجاح');
            return redirect(env("CMS_NAME")."/all-customer");

        }catch (\Exception $e){
            Alert::error('Error Message', 'Optional Title');
            return \Redirect::back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $pages=Customer::find($id);
            return view("dashboard.customer.edit",compact("pages"));
        }catch (\Exception $exception){
            return  \Redirect::back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try {
            $page = Customer::find($request->get("id"));
            $page['title'] = $request->get('title');
            $page['description'] = $request->get('description');
            $page['type'] ="Customer";
            if($request->file('image') != null){
                $image = $request->file('image');
                $filename = time() . '.' . $image->getClientOriginalExtension();
                $path = public_path('asset/images/');
                $image->move($path, $filename);
                $page['image'] = $filename;
            }
            $page->save();
            alert()->success('شكرا لك','تم تعديل معلومات الزبون ');
            return redirect(env("CMS_NAME")."/all-customer");

        } catch (\Exception $e) {
            alert()->error(' حدث خطأ','لم يتم تعديل الزبون ');

        }
    }
    public function updateContent(Request $request){
        try {
            dd($request->all());
            $page = Customer::find($request->get("id"));
            $page['title'] = $request->get('title');
            $page['description'] = $request->get('description');
            $page['type'] ="Customer";
            if($request->file('image') != null){
                $image = $request->file('image');
                $filename = time() . '.' . $image->getClientOriginalExtension();
                $path = public_path('asset/images/');
                $image->move($path, $filename);
                $page['image'] = $filename;
            }
            $page->save();
            Alert::success('Success Message', 'تم اضافة الزبون بنجاح');
            return redirect(env("CMS_NAME")."/all-customer");

        }catch (\Exception $e){
            Alert::error('Error Message', 'Optional Title');
            return \Redirect::back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $page = Customer::find($id);
            $page->delete();
            alert()->success('شكرا لك','تم حذف الزبون ');
            return redirect(env("CMS_NAME")."/all-customer");
        } catch (\Exception $e) {
            alert()->error('شكرا لك','لم يتم حذف الزبون ');

        }
    }
}

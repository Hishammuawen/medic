@extends('master')
@section('content')
    <div id="page-wrapper">
<div class="row">
            <div class="col-lg-12">
<h1 style="margin:0px; padding:15px;" class="page-header">إدارة الموقع الإلكتروني</h1><!-- /.col-lg-12 -->
        </div>
        <div class="modal-body">

            <p style="font-size: 150%">إدارة المستخدمين </p>




            <form action="{{asset(url(env('CMS_NAME').'/update-user/'.$user->id))}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="col-lg-4">
                <div class="form-group">
                    <label for="recipient-name" class="col-form-label">الاسم</label>
                    <input type="text" class="form-control" id="recipient-name" name="name" value={{$user->name}}>
                </div>
                </div>

                <div class="col-lg-4">
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">الصلاحيات</label>
                        <select name="role" class="form-control" required>
                            @foreach($role as $r)
                                @if($user->role == $r->id)
                                <option value="{{$r->id}}" selected>{{$r->name}} </option>
                                @else
                                 <option value="{{$r->id}}">{{$r->name}} </option>
                                @endif
                            @endforeach
                        </select>                      </div>
                </div>
                <div class="col-lg-4">
                <div class="form-group">
                    <label for="recipient-name" class="col-form-label">الايميل</label>
                    <input type="text" class="form-control" id="recipient-name" name="email" value={{$user->email}}>
                </div>
                </div>
               

                <div style="text-align: right" class="modal-footer">
                    <button type="submit" class="btn btn-primary">حفظ</button>
                </div>
            </form>
        </div>
    </div>
@stop



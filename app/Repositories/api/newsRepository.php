<?php


namespace App\Repositories\api;

use App\Exceptions\AppException;
use App\Models\News;
use App\Repositories\BaseRepository;
use DB;
use Exception;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;


class newsRepository extends BaseRepository
{
    public function model()
    {
        return News::class;
    }


    public function relations()
    {
        return [
            //
        ];
    }


    public function create(array $data)
    {

        $news = parent::create($data);

        return  $news;
    }

    public function updateById($id, array $data, array $options = [])
    {
        $news = parent::updateById($id, $data);
        return  $news->refresh();
    }

}

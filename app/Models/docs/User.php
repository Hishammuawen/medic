<?php

/**
 * @apiDefine UsersIndexSearchParams
 * @apiParam {string}  [first_name]   Search By User's First Name
 * @apiParam {string}  [last_name]   Search By User's Last Name
 * @apiParam {string}  [email]   Search By User's Email Address
 * @apiParam {string}  [roles_name]   Search By User's Role Name
 * @apiParam {string}  [roles_display_name]   Search By User's Role Display Name
 * @apiParam {string}   [sortBy]    Property to Sort By
 * @apiParam {string="asc", "desc"} [sortDir]   Sort Direction
 */

/**
 * @apiDefine UsersExample
 * @apiSuccessExample {json} OK
 * HTTP/1.1 200 OK
 * {
 *     "data": [
 *         {
 *             "id": 3,
 *             "first_name": "Fayez",
 *             "last_name": "Ghazzawi",
 *             "email": "fayez.ghazzawi@boilerplate.com",
 *             "created_at": "2020-04-07 07:59:15",
 *             "updated_at": "2020-04-07 07:59:15",
 *             "roles": [
 *                 {
 *                     "id": 2,
 *                     "name": "user",
 *                     "display_name": "User",
 *                     "description": "User Role",
 *                     "created_at": "2020-04-06 11:12:07",
 *                     "updated_at": "2020-04-06 11:12:07"
 *                 }
 *             ]
 *         },
 *         {
 *             "id": 1,
 *             "first_name": "admin",
 *             "last_name": "admin",
 *             "email": "admin@boilerplate.com",
 *             "created_at": "2020-04-06 11:12:07",
 *             "updated_at": "2020-04-06 11:28:07",
 *             "roles": [
 *                 {
 *                     "id": 1,
 *                     "name": "admin",
 *                     "display_name": "Administrator",
 *                     "description": "Administrator Role",
 *                     "created_at": "2020-04-06 11:12:07",
 *                     "updated_at": "2020-04-06 11:12:07"
 *                 }
 *             ]
 *         }
 *     ],
 *     "pagination": {
 *         "total_count": 2,
 *         "total_pages": 1,
 *         "current_page": 1,
 *         "limit": 2
 *     }
 * }
 */

/**
 * @apiDefine UserExample
 * @apiSuccessExample {json} OK
 * HTTP/1.1 200 OK
 * {
 *     "id": 3,
 *     "first_name": "Fayez",
 *     "last_name": "Ghazzawi",
 *     "email": "fayez.ghazzawi@boilerplate.com",
 *     "created_at": "2020-04-07 07:59:15",
 *     "updated_at": "2020-04-07 07:59:15",
 *     "roles": [
 *         {
 *             "id": 2,
 *             "name": "user",
 *             "display_name": "User",
 *             "description": "User Role",
 *             "created_at": "2020-04-06 11:12:07",
 *             "updated_at": "2020-04-06 11:12:07"
 *         }
 *     ]
 * }
 */

/**
 * @apiDefine UserStoreParams
 * @apiParam {string}  first_name   User's First Name
 * @apiParam {string}  last_name   User's Last Name
 * @apiParam {email}  email   User's Email Address
 * @apiParam {string}  password   User's Password
 * @apiParam {array}  roles   User's Array of Roles IDs
 */

/**
 * @apiDefine UserUpdateParams
 * @apiParam {string}  [first_name]   User's New First Name
 * @apiParam {string}  [last_name]   User's New Last Name
 * @apiParam {email}  [email]   User's New Email Address
 * @apiParam {string}  [password]   User's New Password
 * @apiParam {array}  [roles]   User's Array of New Roles IDs
 */

/**
 * @apiDefine UserDestroySuccessExample
 * @apiSuccessExample {json} OK
 * HTTP/1.1 200 OK
 * {
 *     "message_en": "User Deleted Successfully",
 *     "message_ar": "تم حذف الحدث بشكل صحيح"
 * }
 */

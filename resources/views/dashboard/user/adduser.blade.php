@extends('master')
@section('content')
    <div id="page-wrapper">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 style="margin:0px; padding:15px;" class="page-header">إدارة الموقع الإلكتروني</h1><!-- /.col-lg-12 -->
                    </div>
        <div class="modal-body">
            <div class="col-lg-12">
                <h1 style="margin:0px; padding:15px;" class="page-header"> إضافة محرر جديد </h1><!-- /.col-lg-12 -->
            </div>
                        <form action="{{asset(url(env('CMS_NAME').'/save-user'))}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row">
                <div class="col-lg-4">
                <div class="form-group">
                    <label for="recipient-name" class="col-form-label">اسم المحرر</label>
                    <input type="text" class="form-control" id="recipient-name" name="name" required>
                </div>
                </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                        <label for="recipient-name" class="col-form-label">البريد الإلكتروني</label>
                        <input type="email" class="form-control" id="recipient-name" name="email" required>
                        </div>
                </div>
                    <div class="col-lg-4">
                        <div class="form-group">
                        <label for="recipient-name" class="col-form-label">كلمة المرور</label>
                        <input type="text" class="form-control" id="recipient-name" name="password" required>
                        </div>
                    </div>
                    </div>
                    <div class="row">
                    <div class="col-lg-4">
                        <div class="form-group">
                        <label for="message-text" class="col-form-label">الصلاحيات</label>
<select name="role" class="form-control" required>
    @foreach($role as $r)
    <option value="{{$r->id}}">{{$r->name}} </option>
        @endforeach
</select>                      </div>
                  </div>
                  </div>



                <div style="text-align: right" class="modal-footer">

                    <button type="submit" class="btn btn-primary">حفظ</button>
                </div>
            </form>
        </div>














    </div>
@stop


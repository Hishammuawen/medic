
@extends('master',['title'=>"تعديل السجلات"])
@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
<h1 style="margin:0px; padding:15px;" class="page-header">إدارة الموقع الإلكتروني</h1><!-- /.col-lg-12 -->
        </div>

        <div class="modal-body">
            <p style="font-size: 150%">عنصر القائمة </p>
            @if (Session::has('sweet_alert.alert'))
                <script>
                    swal({!! Session::get('sweet_alert.alert') !!});
                </script>
            @endif
            <form action="{{asset(url(env('CMS_NAME').'/update-element'))}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">عربي</label>
                        <input type="text" class="form-control" id="recipient-name" value="{{$menus->title}}" name="title" required>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label"> الرابط</label>
                        <input type="text" class="form-control" id="recipient-name"  value="{{$menus->links}}"name="link" required>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label"> أولوية الظهور</label>
                        <input type="number" class="form-control" id="recipient-name"  value="{{$menus->Priority}}"name="priority" required>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="recipient-name"  class="col-form-label">   أساسي أو فرعي </label>
                        <select name="type" class="form-control">
                            @if($menus->Priority == 1)
                            <option value="1" selected>أساسي</option>
                            <option value="2">فرعي</option>
                                @else
                                <option value="1" >أساسي</option>
                                <option value="2"selected>فرعي</option>
                                @endif
                        </select>
                    </div>
                </div>

                <div style="text-align: right" class="modal-footer">
                    <button type="submit" class="btn btn-primary">حفظ</button>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">

                        <input type="hidden" class="form-control" id="recipient-name" value="{{$menus->id}}" name="id" required>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop

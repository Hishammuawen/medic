
@extends('master')
@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 style="margin:0px; padding:15px;" class="page-header">إدارة الموقع الإلكتروني</h1><!-- /.col-lg-12 -->
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <div style="margin-top: 20px;" class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">


                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>الاسم</th>

                                    <th>البريد الإلكتروني</th>
                                    <th>  العمليات</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($all as $r)
                                    <tr class="odd gradeX">
                                        <td>{{$r->id}}</td>
                                        <td>{{$r->name}}</td>
                                        <td>{{$r->email}}</td>
                                        <td>

                                            <button type="button" class="btn btn-primary btn-circle">
                                                <a style="color: white" href="{{asset(url(env('CMS_NAME').'/edit-user/'.$r->id))}}" aria-labelledby="myModalLabel1" aria-hidden="true">
                                                    <i class="fa fa-bars"></i></a>
                                            </button>

                                            <button type="button" class="btn btn-danger btn-circle"   id="btn-confirm"><a  style="color: white"  href="{{asset(url(env('CMS_NAME').'/delete-user/'.$r->id))}}" aria-labelledby="myModalLabel1" aria-hidden="true"><i class="fa fa-times"></i></a>
                                            </button>

                                            </li>

                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                    <!-- /.panel-body -->
                </div>
                <!-- /.panel -->
            </div>
            <!-- /.col-lg-12 -->
        </div>



@stop



















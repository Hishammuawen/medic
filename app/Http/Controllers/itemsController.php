<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Component;
use App\Item;
use Illuminate\Http\Request;
use Alert;
use Illuminate\Support\Facades\Redirect;

class itemsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(){
        $pages=Item::all();
        return view("dashboard.items.index",compact("pages"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
		$Component=Component::all();
        return  view("dashboard.items.addItem",compact('Component'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    //dd($request->all());
        try {
            $page = new Item();
            $page['title'] = $request->get('title');
            $page['description'] = $request->get('description');
			$page['price'] = $request->get('price');
			$page['component_id'] = $request->get('component_id');

            if($request->file('image') != null){
                $image = $request->file('image');
                $filename = time() . '.' . $image->getClientOriginalExtension();
                $path = public_path('asset/images/');
                $image->move($path, $filename);
                $page['image'] = $filename;
            }
            $page->save();
            Alert::success('Success Message', 'تم إضافة المادة بنجاح');
            return redirect(env("CMS_NAME")."/all-elements-items");

        }catch (\Exception $e){
			dd($e);
            Alert::error('Error Message', 'Optional Title');
            return \Redirect::back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $page=Item::find($id);
			        $pages=Component::all();

            return view("dashboard.items.edit",compact("pages","page"));
        }catch (\Exception $exception){
            return  \Redirect::back();
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
            try {
            $page =  Item::find($request->get("id"));
            $page['title'] = $request->get('title');
            $page['description'] = $request->get('description');
			$page['price'] = $request->get('price');
			$page['component_id'] = $request->get('component_id');

            if($request->file('image') != null){
                $image = $request->file('image');
                $filename = time() . '.' . $image->getClientOriginalExtension();
                $path = public_path('asset/images/');
                $image->move($path, $filename);
                $page['image'] = $filename;
            }
            $page->save();
            Alert::success('Success Message', 'تم إضافة المادة بنجاح');
            return redirect(env("CMS_NAME")."/all-elements-items");

        }catch (\Exception $e){
			dd($e);
            Alert::error('Error Message', 'Optional Title');
            return \Redirect::back();
        }
    }
    public function updateContent(Request $request){
        try {
            dd($request->all());
            $page = Item::find($request->get("id"));
            $page['title'] = $request->get('title');
            $page['description'] = $request->get('description');
            $page['type'] ="Customer";
            if($request->file('image') != null){
                $image = $request->file('image');
                $filename = time() . '.' . $image->getClientOriginalExtension();
                $path = public_path('asset/images/');
                $image->move($path, $filename);
                $page['image'] = $filename;
            }
            $page->save();
            Alert::success('Success Message', 'تم اضافة الزبون بنجاح');
            return redirect(env("CMS_NAME")."/all-elements-items");

        }catch (\Exception $e){
            Alert::error('Error Message', 'Optional Title');
            return \Redirect::back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $page = Item::find($id);
            $page->delete();
            alert()->success('شكرا لك','تم حذف الزبون ');
            return redirect(env("CMS_NAME")."/all-elements-items");
        } catch (\Exception $e) {
            alert()->error('شكرا لك','لم يتم حذف الزبون ');

        }
    }
}

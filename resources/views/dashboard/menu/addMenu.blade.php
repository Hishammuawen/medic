
@extends('master',['title'=>"أضافة عنصر جديد"])
@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
<h1 style="margin:0px; padding:15px;" class="page-header">إدارة الموقع الإلكتروني</h1><!-- /.col-lg-12 -->
        </div>

        <div class="modal-body">
            <p style="font-size: 150%">عنصر القائمة </p>
            @if (Session::has('sweet_alert.alert'))
                <script>
                    swal({!! Session::get('sweet_alert.alert') !!});
                </script>
            @endif
            <form action="{{asset(url(env('CMS_NAME').'/save-element'))}}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label">عربي</label>
                        <input type="text" class="form-control" id="recipient-name" name="title" required>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label"> الرابط</label>
                        <input type="text" class="form-control" id="recipient-name" name="link" required>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="recipient-name" class="col-form-label"> أولوية الظهور</label>
                        <input type="number" class="form-control" id="recipient-name" name="priority" required>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="recipient-name"  class="col-form-label">   أساسي أو فرعي </label>
                        <select name="type" class="form-control">
                            <option value="1">أساسي</option>
                            <option value="2">فرعي</option>
                        </select>
                    </div>
                </div>
                <div style="text-align: right" class="modal-footer">
                    <button type="submit" class="btn btn-primary">حفظ</button>
                </div>
            </form>
        </div>
    </div>
@stop
